from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import pandas as pd
import os

S0 = 100.0 # S0 = Stock price
strikes = [i for i in range(50, 150)] # Exercise prices range
T = 1 # T = Time to expiration
r = 0.01 # r = risk-free interest rate
q = 0.02 # q = dividend yield
vol = 0.2 # vol = volatility
Nsteps = 100  # Number or steps in MC


def phi(x): # Cumulative distribution function for the standard normal distribution
    return (1.0 + erf(x / sqrt(2.0))) / 2.0

# Method 1: Analytical solution
def blackscholes(S0, K, T, r, q, vol): # Formula of Black-Scholes Model for an European Call Option
    d1 = (log(S0 / K) + (r - q + 0.5 * vol ** 2) * T) / (vol * sqrt(T))
    d2 = (log(S0 / K) + (r - q - 0.5 * vol ** 2) * T) / (vol * sqrt(T))
    value = S0 * phi(d1) * exp(-q * T) - K * exp(-r * T) * phi(d2)
    return value

# Method 2: Monte carlo approximation
def montecarlobs(S0, K, T, r, q, vol, Nsteps):
    payoff = 0.0
    sfin = []
    for i in range(Nsteps):
        rnd = random.gauss(0, sqrt(T)) #Random Wiener process
        sfinal = S0 * exp((r - q - 0.5 * vol ** 2) * T + vol * rnd)
        sfin.append(sfinal)
        payoff += max(sfinal - K, 0)
payoff = payoff * exp(-r * T) / float(Nsteps)
return payoff